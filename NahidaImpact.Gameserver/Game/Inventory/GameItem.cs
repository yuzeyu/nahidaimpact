﻿using NahidaImpact.Common.Constants;

namespace NahidaImpact.Gameserver.Game.Inventory;

internal class GameItem
{
    public uint ItemId { get; set; }
    public ulong Guid { get; set; }
    public uint Count { get; set; }

    // Weapon
    private uint _level;
    private uint _exp;
    private uint _totalExp;
    private uint _promoteLevel;

    // Relic
    private uint _mainPropId;
    private List<uint> _propIdList = new();

    public uint InvenItemType { get; private set; }

    public void Initialize()
    {
        if (InvenItemType == ItemType.WEAPON)
        {
            _level = 90;
            _exp = 0;
            _totalExp = 1000;
            _promoteLevel = 6;
        }

        if (InvenItemType == ItemType.RELIQUARY)
        {
            _level = 20;
            _exp = 0;
            _totalExp = 1000;
        }
    }

    public GameItem(uint itemId, ulong guid, uint amount)
    {
        ItemId = itemId;
        Guid = guid;
        Count = amount;
        InvenItemType = ItemType.WEAPON; //Auto detect in future
    }
}
