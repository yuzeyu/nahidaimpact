﻿using System.Diagnostics.CodeAnalysis;
using NahidaImpact.Common.Constants;
using NahidaImpact.Common.Data.Excel;
using NahidaImpact.Gameserver.Game.Avatar;
using NahidaImpact.Gameserver.Game.Entity;
using NahidaImpact.Gameserver.Network.Session;
using NahidaImpact.Protocol;

namespace NahidaImpact.Gameserver.Game;
internal class Player(ExcelTableCollection excelTables, NetSession session, EntityManager entityManager)
{
    private readonly NetSession _session = session;
    private readonly EntityManager _entityManager = entityManager;

    private static readonly uint[] AvatarBlackList = [10000001, 10000007]; // kate and traveler

    public uint Uid { get; set; }
    public uint GuidSeed { get; set; }
    public string Name { get; set; } = "Traveler";

    public List<GameAvatar> Avatars { get; set; } = [];
    public List<GameAvatarTeam> AvatarTeams { get; set; } = [];
    public uint CurTeamIndex { get; set; }

    public uint CurAvatarEntityId { get; set; }
    public Vector LastMainWorldPos { get; set; } = new();

    private readonly ExcelTableCollection _excelTables = excelTables;

    public void InitDefaultPlayer()
    {
        // We don't have database atm, so let's init default player state for every session.

        Uid = 1337;
        Name = "ReversedRooms";

        UnlockAllAvatars();

        _ = TryGetAvatar(10000073, out GameAvatar? avatar);
        AvatarTeams.Add(new()
        {
            AvatarGuidList = [avatar!.Guid],
            Index = 1
        });
        CurTeamIndex = 1;

        LastMainWorldPos = GetDefaultMainWorldPos();
    }

    private Vector GetDefaultMainWorldPos() => new()
    {
        X = 2336.789f,
        Y = 249.98896f,
        Z = -751.3081f
    };


    public GameAvatarTeam GetCurrentTeam()
            => AvatarTeams.Find(team => team.Index == CurTeamIndex)!;

    public bool TryGetAvatar(uint avatarId, [MaybeNullWhen(false)] out GameAvatar avatar)
        => (avatar = Avatars.Find(a => a.AvatarId == avatarId)) != null;

    private void UnlockAllAvatars()
    {
        ExcelTable avatarTable = _excelTables.GetTable(ExcelType.Avatar);
        for (int i = 0; i < avatarTable.Count; i++)
        {
            AvatarExcel avatarExcel = avatarTable.GetItemAt<AvatarExcel>(i);
            if (AvatarBlackList.Contains(avatarExcel.Id) || avatarExcel.Id >= 11000000) continue;

            uint currentTimestamp = (uint)DateTimeOffset.Now.ToUnixTimeSeconds();
            GameAvatar avatar = new()
            {
                AvatarId = avatarExcel.Id,
                SkillDepotId = avatarExcel.SkillDepotId,
                WeaponId = avatarExcel.InitialWeapon,
                BornTime = currentTimestamp,
                Guid = NextGuid(),
                WearingFlycloakId = 140001,
                WeaponGuid = NextGuid()
            };

            avatar.InitDefaultProps(avatarExcel);
            Avatars.Add(avatar);
        }
    }

    public async Task sendPlayerData(Player player)
    {
        await _session.NotifyAsync(CmdType.PlayerDataNotify, new PlayerDataNotify
        {
            NickName = player.Name,
            PropMap =
            {
                {PlayerProp.PROP_PLAYER_LEVEL, new() { Type = PlayerProp.PROP_PLAYER_LEVEL, Ival = 60 } },
                {PlayerProp.PROP_PLAYER_WORLD_LEVEL, new() { Type = PlayerProp.PROP_PLAYER_WORLD_LEVEL, Ival = 8 } },
                {PlayerProp.PROP_IS_FLYABLE, new() { Type = PlayerProp.PROP_IS_FLYABLE, Ival = 1 } },
                {PlayerProp.PROP_MAX_STAMINA, new() { Type = PlayerProp.PROP_MAX_STAMINA, Ival = 10000 } },
                {PlayerProp.PROP_CUR_PERSIST_STAMINA, new() { Type = PlayerProp.PROP_CUR_PERSIST_STAMINA, Ival = 10000 } },
                {PlayerProp.PROP_IS_TRANSFERABLE, new() { Type = PlayerProp.PROP_IS_TRANSFERABLE, Ival = 1 } },
                {PlayerProp.PROP_IS_SPRING_AUTO_USE, new() { Type = PlayerProp.PROP_IS_SPRING_AUTO_USE, Ival = 1 } },
                {PlayerProp.PROP_SPRING_AUTO_USE_PERCENT, new() { Type = PlayerProp.PROP_SPRING_AUTO_USE_PERCENT, Ival = 50 } },
                {PlayerProp.PROP_PLAYER_MCOIN, new() { Type = PlayerProp.PROP_PLAYER_MCOIN, Ival = 1 } }
            }
        });
    }

    public ulong NextGuid()
    {
        return ((ulong)Uid << 32) + (++GuidSeed);
    }

    public async ValueTask UnlockTransPoint(uint sceneId, uint pointId)
    {
        ScenePointUnlockNotify scenePointUnlockNotify = new()
        {
            SceneId = sceneId,
        };
        scenePointUnlockNotify.PointList.Add(pointId);

        await _session.NotifyAsync(CmdType.ScenePointUnlockNotify, scenePointUnlockNotify);        
    }

    public Vector GetPosition()
    {
        SceneEntity entity = _entityManager.GetEntityById(CurAvatarEntityId)!;
        return entity.MotionInfo.Pos;
    }
}
