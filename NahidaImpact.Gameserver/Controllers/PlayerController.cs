﻿using NahidaImpact.Common.Data.Binout.Scene.Point;
using NahidaImpact.Common.Data.Binout;
using NahidaImpact.Gameserver.Controllers.Attributes;
using NahidaImpact.Gameserver.Controllers.Result;
using NahidaImpact.Gameserver.Game;
using NahidaImpact.Gameserver.Game.Scene;
using NahidaImpact.Protocol;
using NahidaImpact.Gameserver.Network.Session;
using NahidaImpact.Gameserver.Game.Avatar;

namespace NahidaImpact.Gameserver.Controllers;

[NetController]
internal class PlayerController : ControllerBase
{
    public static List<uint> spawnedEntity = new();

    [NetCommand(CmdType.SetNameCardReq)]
    public ValueTask<IResult> OnSetNameCardReq()
    {
        SetNameCardReq req = Packet!.DecodeBody<SetNameCardReq>();

        return ValueTask.FromResult(Response(CmdType.SetNameCardRsp, new SetNameCardRsp
        {
            NameCardId = req.NameCardId,
            Retcode = 0
        }));
    }

    [NetCommand(CmdType.UnlockTransPointReq)]
    public async ValueTask<IResult> OnUnlockTransPointReq(Player player, BinDataCollection binDataCollection)
    {
        UnlockTransPointReq request = Packet!.DecodeBody<UnlockTransPointReq>();
        uint pointId = request.PointId;

        await player.UnlockTransPoint(request.SceneId, pointId);

        Vector pos = player.GetPosition();
        PointData data = new PointData()
        {
            X = pos.X,
            Y = pos.Y,
            Z = pos.Z
        };
        binDataCollection.points.Add(pointId.ToString(), data);
        binDataCollection.AddPoint(pointId.ToString(), data);

        return Ok();
    }

    [NetCommand(CmdType.GetPlayerFriendListReq)]
    public ValueTask<IResult> OnGetPlayerFriendListReq()
    {
        GetPlayerFriendListRsp rsp = new()
        {
            Retcode = 0
        };

        FriendBrief serverFriend = new()
        {
            Uid = 1,
            Nickname = "Nahida",
            Level = 60,
            ProfilePicture = new()
            {
                AvatarId = 10000073,
            },
            WorldLevel = 8,
            Signature = "Welcome To Nahida Impact",
            LastActiveTime = (uint)(DateTimeOffset.Now.ToUnixTimeMilliseconds() / 1000f),
            NameCardId = 210140,
            OnlineState = FriendOnlineState.FriendOnline,
            Param = 1,
            IsGameSource = true,
            PlatformType = PlatformType.Pc,
            FriendEnterHomeOption = FriendEnterHomeOption.Refuse
        };

        rsp.FriendList.Add(serverFriend);

        return ValueTask.FromResult(Response(CmdType.GetPlayerFriendListRsp, rsp));
    }

    [NetCommand(CmdType.PullPrivateChatReq)]
    public async ValueTask<IResult> OnPullRecentChatReq(Player player, NetSession session)
    {
        PrivateChatNotify privateChatNotify = new()
        {
            ChatInfo = new()
            {
                Time = (uint)(DateTimeOffset.Now.ToUnixTimeMilliseconds() / 1000f),
                ToUid = player.Uid,
                Uid = 1,
                Text = "Welcome to Nahida Impact",
                IsRead = false,
            }
        };
        await session.NotifyAsync(CmdType.PrivateChatNotify, privateChatNotify);
        return Ok();
    }

    [NetCommand(CmdType.PrivateChatReq)]
    public async ValueTask<IResult> OnPrivateChatReq(SceneManager sceneManager)
    {
        PrivateChatReq privateChatReq = Packet!.DecodeBody<PrivateChatReq>();
        string text = privateChatReq.Text;

        if (privateChatReq.HasText && privateChatReq.TargetUid == 1)
        {
            if (!text.StartsWith("/")) return Ok();

            string command = text.Split(" ")[0].Substring(1);
            string[]? args = text.Split(" ")?.Skip(1).ToArray();
            bool hasArgs = args?.Length > 0;

            if (command == "spawn")
            {
                if (hasArgs)
                {
                    uint id = (uint) new Random().Next();
                    await sceneManager.SpawnEntity(uint.Parse(args![0]), id);
                    spawnedEntity.Add(id);

                    Console.WriteLine($"Spawned entity with id {id}");
                }
            }
            else if (command == "killall")
            {
                foreach (uint id in spawnedEntity)
                {
                    await sceneManager.KillEntity(id);
                }

                Console.WriteLine("Killed all spawned entities.");
            }
            else if (command == "kill")
            {
                if(hasArgs)
                {
                    uint id = uint.Parse(args![0]);
                    await sceneManager.KillEntity(id);
                    Console.WriteLine($"Killed entity with id {id}");
                }
            }
            else if (command == "tp" || command == "teleport")
            {
                if (args?.Length == 3)
                {
                    await sceneManager.TeleportTo(float.Parse(args[0]), float.Parse(args[1]), float.Parse(args[2]));
                }
            }
        }

        return Ok();
    }

    [NetCommand(CmdType.PlayerLoginReq)]
    public async ValueTask<IResult> OnPlayerLoginReq(NetSession session, Player player, SceneManager sceneManager)
    {
        player.InitDefaultPlayer();

        await player.sendPlayerData(player);

        AvatarDataNotify avatarDataNotify = new()
        {
            CurAvatarTeamId = player.CurTeamIndex,
            ChooseAvatarGuid = 228
        };

        foreach (GameAvatar gameAvatar in player.Avatars)
        {
            avatarDataNotify.AvatarList.Add(gameAvatar.AsAvatarInfo());
        }

        foreach (GameAvatarTeam team in player.AvatarTeams)
        {
            AvatarTeam avatarTeam = new();
            avatarTeam.AvatarGuidList.AddRange(team.AvatarGuidList);

            avatarDataNotify.AvatarTeamMap.Add(team.Index, avatarTeam);
        }

        await session.NotifyAsync(CmdType.AvatarDataNotify, avatarDataNotify);

        await session.NotifyAsync(CmdType.OpenStateUpdateNotify, new OpenStateUpdateNotify
        {
            OpenStateMap =
            {
                {1, 1},
                {2, 1},
                {3, 1},
                {4, 1},
                {5, 1},
                {6, 1},
                {7, 0},
                {8, 1},
                {10, 1},
                {11, 1},
                {12, 1},
                {13, 1},
                {14, 1},
                {15, 1},
                {27, 1},
                {28, 1},
                {29, 1},
                {30, 1},
                {31, 1},
                {32, 1},
                {33, 1},
                {37, 1},
                {38, 1},
                {45, 1},
                {47, 1},
                {48, 0},
                {53, 1},
                {54, 1},
                {55, 1},
                {59, 1},
                {62, 1},
                {65, 1},
                {900, 1},
                {901, 1},
                {902, 1},
                {903, 1},
                {1001, 1},
                {1002, 1},
                {1003, 1},
                {1004, 1},
                {1005, 1},
                {1007, 1},
                {1008, 1},
                {1009, 1},
                {1010, 1},
                {1100, 1},
                {1103, 1},
                {1300, 1},
                {1401, 1},
                {1403, 1},
                {1700, 1},
                {2100, 1},
                {2101, 1},
                {2103, 1},
                {2400, 1},
                {3701, 1},
                {3702, 1},
                {4100, 1 }
            }
        });

        await sceneManager.EnterSceneAsync(3);

        return Response(CmdType.PlayerLoginRsp, new PlayerLoginRsp
        {
            CountryCode = "RU",
            GameBiz = "hk4e_global",
            ResVersionConfig = new()
        });
    }
}
