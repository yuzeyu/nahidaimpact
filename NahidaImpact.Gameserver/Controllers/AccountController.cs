﻿using NahidaImpact.Common.Constants;
using NahidaImpact.Common.Security;
using NahidaImpact.Gameserver.Controllers.Attributes;
using NahidaImpact.Gameserver.Controllers.Result;
using NahidaImpact.Gameserver.Game;
using NahidaImpact.Gameserver.Game.Avatar;
using NahidaImpact.Gameserver.Game.Scene;
using NahidaImpact.Gameserver.Network.Session;
using NahidaImpact.Protocol;

namespace NahidaImpact.Gameserver.Controllers;

[NetController]
internal class AccountController : ControllerBase
{
    [NetCommand(CmdType.GetPlayerTokenReq)]
    public ValueTask<IResult> OnGetPlayerTokenReq()
    {
        return ValueTask.FromResult(Response(CmdType.GetPlayerTokenRsp, new GetPlayerTokenRsp
        {
            ServerRandKey = "CfO2d7eEYha5bJRXdCfoiemPNAtXDpyNTQ3ObeTt5a7SSHz6GAEO1WPiTQ7fR6OG8LqhVN3ZTxH9Bnkc09BnCxud+kn0+PiGv1PTOuWK0LkQQ1xmg89zA9IHS+OJd1yKT2BBmJf4sN61gi+WtT7aFwRlzku3kGCk6p2wiPo2enE7UwCFi/GiD4vq/m3hNZiKBjitAvheaqbSLjMpBax+c8HXoY5G09ap1PjEnUQPIK0xZRRQKpnrWcCyP4j8N3WwYYQGDW+OYOJjBvJdv+D6XSdEi+4IsZASYVpu9V8UZ570Cakbc+IjUm0UZJXghcR7izIjKtoNHf2Fmc26DEp1Jw==",
Sign = "mMx/Klovbzq1QxQvVgm30nYhj0jDOykyo9aparyWRNz3ACxV/2gIdLpyM/SMerWMTcx26NapQ9HsKK7BRK7Yx+nMR0O83BkBlxfl+NEarYr6kj9lBKAxZYXTXFRYA4sRynvwa/MOPmGwYMNl6aVvMohhvrsTopsRvIuGFtnCVL2wBfbxcNnbVfP5k+DxPuQnxa/vi+ju8TogW2R+r0p9zQ5NJe1oaYe4xYbyhefFVv11FA/JQHwMHLEyrEdPqTzdN75CUmE09yLuAoeJzoJ1vwwjwfcH9dMDPxsewNJBGiylVHYf56kF4HypNkYNjtxbghgLBaHg0ZoeYHTOJ7YUTQ==",
            Uid = 1337,
            CountryCode = "RU",
            PlatformType = 3
        }));
    }

    [NetCommand(CmdType.PingReq)]
    public ValueTask<IResult> OnPingReq()
    {
        return ValueTask.FromResult(Response(CmdType.PingRsp, new PingRsp
        {
            //ServerTime = (uint)DateTimeOffset.Now.ToUnixTimeSeconds()
        }));
    }

    [NetCommand(CmdType.SetPlayerNameReq)]
    public async ValueTask<IResult> OnSetPlayerNameReq(Player player)
    {
        SetPlayerNameReq req = Packet!.DecodeBody<SetPlayerNameReq>();

        player.Name = req.NickName;
        await player.sendPlayerData(player);
        return Response(CmdType.SetPlayerNameRsp, new SetPlayerNameRsp
        {
            NickName = req.NickName,
        });
    }

    [NetCommand(CmdType.GetAllUnlockNameCardReq)]
    public ValueTask<IResult> OnGetAllUnlockNameCardReq()
    {
        GetAllUnlockNameCardRsp rsp = new()
        {
            NameCardList =
            {
                210002
            }
        };
        return ValueTask.FromResult(Response(CmdType.GetAllUnlockNameCardRsp, rsp));
    }
}
