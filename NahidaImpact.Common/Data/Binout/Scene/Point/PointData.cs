﻿using System.Text.Json.Serialization;

namespace NahidaImpact.Common.Data.Binout.Scene.Point;

public class PointData
{
    [JsonPropertyName("_x")]
    public float X { get; set; }

    [JsonPropertyName("_y")]
    public float Y { get; set; }

    [JsonPropertyName("_z")]
    public float Z { get; set; }
}
