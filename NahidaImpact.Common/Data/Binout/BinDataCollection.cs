﻿using System.Collections.Immutable;
using System.Text.Json;
using NahidaImpact.Common.Data.Provider;
using Microsoft.Extensions.Logging;
using NahidaImpact.Common.Data.Binout.Scene.Point;
using System.Text.Json.Nodes;

namespace NahidaImpact.Common.Data.Binout;
public class BinDataCollection
{
    private static IAssetProvider? _assetProvider;
    private readonly ImmutableDictionary<uint, AvatarConfig> _avatarConfigs;
    public ImmutableDictionary<string, PointData> points;

    public BinDataCollection(IAssetProvider assetProvider, ILogger<BinDataCollection> logger, DataHelper dataHelper)
    {
        _assetProvider = assetProvider;
        _avatarConfigs = LoadAvatarConfigs(dataHelper);
        points = LoadPointData();

        logger.LogInformation("Loaded {count} avatar configs", _avatarConfigs.Count);
    }

    public AvatarConfig GetAvatarConfig(uint id)
    {
        return _avatarConfigs[id];
    }

    public PointData GetPointData(uint id)
    {
        return points[id.ToString()];
    }

    private static ImmutableDictionary<uint, AvatarConfig> LoadAvatarConfigs(DataHelper dataHelper)
    {
        ImmutableDictionary<uint, AvatarConfig>.Builder builder = ImmutableDictionary.CreateBuilder<uint, AvatarConfig>();
        IEnumerable<string> avatarConfigFiles = _assetProvider!.EnumerateAvatarConfigFiles();

        foreach (string avatarConfigFile in avatarConfigFiles)
        {
            string avatarName = avatarConfigFile[(avatarConfigFile.LastIndexOf('_') + 1)..];
            avatarName = avatarName.Remove(avatarName.IndexOf('.'));

            if (dataHelper.TryResolveAvatarIdByName(avatarName, out uint id)) 
            {
                JsonDocument configJson = _assetProvider.GetFileAsJsonDocument(avatarConfigFile);
                if (configJson.RootElement.ValueKind != JsonValueKind.Object)
                    throw new JsonException($"BinDataCollection::LoadAvatarConfigs - expected an object, got {configJson.RootElement.ValueKind}");

                AvatarConfig avatarConfig = configJson.RootElement.Deserialize<AvatarConfig>()!;
                builder.Add(id, avatarConfig);
            }
            else
            {
                throw new KeyNotFoundException($"BinDataCollection::LoadAvatarConfigs - failed to resolve avatar id for {avatarName}");
            }
        }

        return builder.ToImmutable();
    }

    private static ImmutableDictionary<string, PointData> LoadPointData()
    {
        ImmutableDictionary<string, PointData>.Builder builder = ImmutableDictionary.CreateBuilder<string, PointData>();
        Dictionary<string, Dictionary<string, object>> jsonDict = JsonSerializer.Deserialize<Dictionary<string, Dictionary<string, object>>>(_assetProvider!.GetScenePointFile())!;
        Dictionary<string, object> OKJIDAEFIOD = jsonDict["OKJIDAEFIOD"];
        foreach (var entry in OKJIDAEFIOD)
        {
            string key = entry.Key;
            var value = entry.Value;

            builder.Add(key, JsonNode.Parse(value.ToString()!)!.AsObject()["GKOBOKNGMNE"].Deserialize<PointData>()!);
        }

        return builder.ToImmutable();
    }

    public void AddPoint(string id, PointData data)
    {
        JsonObject scene = _assetProvider!.GetScenePointFile();
        JsonObject pointData = new()
        {
            { "_x", data.X },
            { "_y", data.Y },
            { "_z", data.Z },
        };
        JsonObject GKOBOKNGMNE = new()
        {
            { "GKOBOKNGMNE", pointData },
        };
        scene["OKJIDAEFIOD"]!.AsObject().Add(id, GKOBOKNGMNE);
        _assetProvider.SaveScenePointFile(scene);
    }
}
