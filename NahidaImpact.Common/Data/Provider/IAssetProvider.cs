﻿using System.Text.Json;
using System.Text.Json.Nodes;

namespace NahidaImpact.Common.Data.Provider;
public interface IAssetProvider
{
    JsonDocument GetExcelTableJson(string assetName);
    IEnumerable<string> EnumerateAvatarConfigFiles();
    JsonDocument GetFileAsJsonDocument(string fullPath);

    // Point
    JsonObject GetScenePointFile();
    void SaveScenePointFile(JsonObject data);
}
