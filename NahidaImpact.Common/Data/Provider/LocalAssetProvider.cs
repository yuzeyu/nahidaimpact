﻿using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace NahidaImpact.Common.Data.Provider;
internal sealed class LocalAssetProvider : IAssetProvider
{
    private const string ExcelDirectory = "assets/excel/";
    private const string AvatarConfigDirectory = "assets/binout/avatar/";
    private const string ScenePointDirectory = "assets/binout/scene/point/scene3_point.json";

    public IEnumerable<string> EnumerateAvatarConfigFiles()
    {
        return Directory.GetFiles(AvatarConfigDirectory);
    }

    public JsonDocument GetFileAsJsonDocument(string fullPath)
    {
        using FileStream fileStream = new(fullPath, FileMode.Open, FileAccess.Read);
        return JsonDocument.Parse(fileStream);
    }

    public JsonDocument GetExcelTableJson(string assetName)
    {
        string filePath = string.Concat(ExcelDirectory, assetName);
        using FileStream fileStream = new(filePath, FileMode.Open, FileAccess.Read);

        return JsonDocument.Parse(fileStream);
    }

    public JsonObject GetScenePointFile()
    {
        using FileStream fileStream = new(ScenePointDirectory, FileMode.Open, FileAccess.Read);
        return JsonNode.Parse(fileStream)!.AsObject();
    }

    public void SaveScenePointFile(JsonObject data)
    {
        string output = data.ToJsonString(new JsonSerializerOptions { WriteIndented = true, Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping });
        File.WriteAllText(ScenePointDirectory, output, Encoding.UTF8);
    }
}
