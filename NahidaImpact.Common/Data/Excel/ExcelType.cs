﻿namespace NahidaImpact.Common.Data.Excel;
public enum ExcelType
{
    Avatar,
    Weapon,
    Material,
    Reliquary
}
