﻿using System.Text.Json;
using System.Text.Json.Serialization;
using System.Globalization;
using NahidaImpact.Common.Data.Excel.Attributes;

namespace NahidaImpact.Common.Data.Excel.Inventory;

[Excel(ExcelType.Material, "MaterialExcelConfigData.json")]
public partial class MaterialExcel :ExcelItem
{
    public override uint ExcelId => Id;

    [JsonPropertyName("interactionTitleTextMapHash")]
    public long InteractionTitleTextMapHash { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("noFirstGetHint")]
    public bool? NoFirstGetHint { get; set; }

    [JsonPropertyName("itemUse")]
    public List<ItemUse> ItemUse { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("rankLevel")]
    public long? RankLevel { get; set; }

    [JsonPropertyName("effectDescTextMapHash")]
    public long EffectDescTextMapHash { get; set; }

    [JsonPropertyName("specialDescTextMapHash")]
    public long SpecialDescTextMapHash { get; set; }

    [JsonPropertyName("typeDescTextMapHash")]
    public long TypeDescTextMapHash { get; set; }

    [JsonPropertyName("effectIcon")]
    public string EffectIcon { get; set; }

    [JsonPropertyName("effectName")]
    public string EffectName { get; set; }

    [JsonPropertyName("picPath")]
    public List<string> PicPath { get; set; }

    [JsonPropertyName("satiationParams")]
    public List<long> SatiationParams { get; set; }

    [JsonPropertyName("destroyReturnMaterial")]
    public List<object> DestroyReturnMaterial { get; set; }

    [JsonPropertyName("destroyReturnMaterialCount")]
    public List<object> DestroyReturnMaterialCount { get; set; }

    [JsonPropertyName("id")]
    public uint Id { get; set; }

    [JsonPropertyName("nameTextMapHash")]
    public long NameTextMapHash { get; set; }

    [JsonPropertyName("descTextMapHash")]
    public long DescTextMapHash { get; set; }

    [JsonPropertyName("icon")]
    public string Icon { get; set; }

    [JsonPropertyName("itemType")]
    public string ItemType { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("rank")]
    public long? Rank { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("effectGadgetID")]
    public long? EffectGadgetId { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("materialType")]
    public string MaterialType { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("gadgetId")]
    public long? GadgetId { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("isForceGetHint")]
    public bool? IsForceGetHint { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("stackLimit")]
    public long? StackLimit { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("maxUseCount")]
    public long? MaxUseCount { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("useOnGain")]
    public bool? UseOnGain { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("useTarget")]
    public string UseTarget { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("useLevel")]
    public long? UseLevel { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("isSplitDrop")]
    public bool? IsSplitDrop { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("destroyRule")]
    public string DestroyRule { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("weight")]
    public long? Weight { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("setID")]
    public long? SetId { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("closeBagAfterUsed")]
    public bool? CloseBagAfterUsed { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("foodQuality")]
    public string FoodQuality { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("FCIEMIPHKKG")]
    public string Fciemiphkkg { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("globalItemLimit")]
    public long? GlobalItemLimit { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("cdTime")]
    public long? CdTime { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("cdGroup")]
    public long? CdGroup { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("playGainEffect")]
    public bool? PlayGainEffect { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("isHidden")]
    public bool? IsHidden { get; set; }
}

public partial class ItemUse
{
    [JsonPropertyName("useParam")]
    public List<string> UseParam { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("useOp")]
    public string UseOp { get; set; }
}