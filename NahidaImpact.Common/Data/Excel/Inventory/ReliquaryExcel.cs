﻿using NahidaImpact.Common.Data.Excel.Attributes;
using System.Text.Json.Serialization;

namespace NahidaImpact.Common.Data.Excel.Inventory;

[Excel(ExcelType.Reliquary, "ReliquaryExcelConfigData.json")]
public partial class ReliquaryExcel : ExcelItem
{
    public override uint ExcelId => Id;

    [JsonPropertyName("equipType")]
    public string EquipType { get; set; }

    [JsonPropertyName("showPic")]
    public string ShowPic { get; set; }

    [JsonPropertyName("rankLevel")]
    public long RankLevel { get; set; }

    [JsonPropertyName("mainPropDepotId")]
    public long MainPropDepotId { get; set; }

    [JsonPropertyName("appendPropDepotId")]
    public long AppendPropDepotId { get; set; }

    [JsonPropertyName("addPropLevels")]
    public List<long> AddPropLevels { get; set; }

    [JsonPropertyName("baseConvExp")]
    public long BaseConvExp { get; set; }

    [JsonPropertyName("maxLevel")]
    public long MaxLevel { get; set; }

    [JsonPropertyName("destroyReturnMaterial")]
    public List<long> DestroyReturnMaterial { get; set; }

    [JsonPropertyName("destroyReturnMaterialCount")]
    public List<long> DestroyReturnMaterialCount { get; set; }

    [JsonPropertyName("id")]
    public uint Id { get; set; }

    [JsonPropertyName("nameTextMapHash")]
    public long NameTextMapHash { get; set; }

    [JsonPropertyName("descTextMapHash")]
    public long DescTextMapHash { get; set; }

    [JsonPropertyName("icon")]
    public string Icon { get; set; }

    [JsonPropertyName("itemType")]
    public string ItemType { get; set; }

    [JsonPropertyName("weight")]
    public long Weight { get; set; }

    [JsonPropertyName("rank")]
    public long Rank { get; set; }

    [JsonPropertyName("gadgetId")]
    public long GadgetId { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("appendPropNum")]
    public long? AppendPropNum { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("setId")]
    public long? SetId { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("storyId")]
    public long? StoryId { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("destroyRule")]
    public string DestroyRule { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("dropable")]
    public bool? Dropable { get; set; }
}
