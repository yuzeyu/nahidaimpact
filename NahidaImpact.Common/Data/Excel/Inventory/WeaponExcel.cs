﻿using System.Text.Json;
using System.Text.Json.Serialization;
using System.Globalization;
using NahidaImpact.Common.Data.Excel.Attributes;

namespace NahidaImpact.Common.Data.Excel.Inventory;

[Excel(ExcelType.Weapon, "WeaponExcelConfigData.json")]
public partial class WeaponExcel : ExcelItem
{
    public override uint ExcelId => Id;

    [JsonPropertyName("weaponType")]
    public string WeaponType { get; set; }

    [JsonPropertyName("rankLevel")]
    public long RankLevel { get; set; }

    [JsonPropertyName("weaponBaseExp")]
    public long WeaponBaseExp { get; set; }

    [JsonPropertyName("skillAffix")]
    public List<long> SkillAffix { get; set; }

    [JsonPropertyName("weaponProp")]
    public List<WeaponProp> WeaponProp { get; set; }

    [JsonPropertyName("awakenTexture")]
    public string AwakenTexture { get; set; }

    [JsonPropertyName("awakenLightMapTexture")]
    public string AwakenLightMapTexture { get; set; }

    [JsonPropertyName("awakenIcon")]
    public string AwakenIcon { get; set; }

    [JsonPropertyName("weaponPromoteId")]
    public long WeaponPromoteId { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("storyId")]
    public long? StoryId { get; set; }

    [JsonPropertyName("awakenCosts")]
    public List<long> AwakenCosts { get; set; }

    [JsonPropertyName("gachaCardNameHash")]
    public double GachaCardNameHash { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("destroyRule")]
    public string DestroyRule { get; set; }

    [JsonPropertyName("destroyReturnMaterial")]
    public List<long> DestroyReturnMaterial { get; set; }

    [JsonPropertyName("destroyReturnMaterialCount")]
    public List<long> DestroyReturnMaterialCount { get; set; }

    [JsonPropertyName("id")]
    public uint Id { get; set; }

    [JsonPropertyName("nameTextMapHash")]
    public long NameTextMapHash { get; set; }

    [JsonPropertyName("descTextMapHash")]
    public long DescTextMapHash { get; set; }

    [JsonPropertyName("icon")]
    public string Icon { get; set; }

    [JsonPropertyName("itemType")]
    public string ItemType { get; set; }

    [JsonPropertyName("weight")]
    public long Weight { get; set; }

    [JsonPropertyName("rank")]
    public long Rank { get; set; }

    [JsonPropertyName("gadgetId")]
    public long GadgetId { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("initialLockState")]
    public long? InitialLockState { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("awakenMaterial")]
    public long? AwakenMaterial { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("enhanceRule")]
    public long? EnhanceRule { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("unRotate")]
    public bool? UnRotate { get; set; }
}

public partial class WeaponProp
{
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("propType")]
    public string PropType { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("initValue")]
    public double? InitValue { get; set; }

    [JsonPropertyName("type")]
    public string Type { get; set; }
}
