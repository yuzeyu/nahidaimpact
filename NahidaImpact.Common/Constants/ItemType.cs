﻿namespace NahidaImpact.Common.Constants;

public static class ItemType
{
    public const uint VIRTUAL = 0;
    public const uint MATERIAL = 1;
    public const uint WEAPON = 2;
    public const uint RELIQUARY = 3; //These number should be changed
}
