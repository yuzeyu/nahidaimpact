﻿namespace NahidaImpact.Common.Constants;

public static class EquipType
{
    public const uint NONE = 0;
    public const uint BRACER = 1;
    public const uint NECKLACE = 2;
    public const uint SHOES = 3;
    public const uint RING = 4;
    public const uint DRESS = 5;
    public const uint WEAPON = 6;
}
